# OpenML dataset: Pokemon-with-stats-Generation-8

https://www.openml.org/d/43692

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
After the release of Pokmon Sword and Shield on the November 15, 2019, 81 new Pokemons were released alongside 13 regional variants of preexisting Pokmon.
Content
Like dataset from previous generations, you'll find data about:

Name 
HP
Attack     
Defense     
Sp. Attack     
Sp. Defense     
Speed     
Total     
Average
Generation

Acknowledgements
Alberto Barradas for all the Pokemons until generation 6 and gurarako for the generation 7.
Additional information gathered thanks to bulbapedia.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43692) of an [OpenML dataset](https://www.openml.org/d/43692). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43692/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43692/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43692/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

